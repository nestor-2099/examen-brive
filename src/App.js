import React, {Component} from 'react';

import './App.css';

// React Deployment
// https://jsramblings.com/continuously-deploy-a-react-app-to-gitlab-pages/

// Camera Settings
// https://developer.mozilla.org/es/docs/Web/API/WebRTC_API/Taking_still_photos
var width = 320;
var height = 0;
var streaming = false;
var video = null;
var canvas = null;

class App extends Component {
  constructor(props) {
		super(props);
		this.state = {
      employees: [],
      newEmployee: false,
      newEmployeeName: '',
      newEmployeeEnterprise: '',
      newEmployeeSalary: '',
      filter: '',
      currency: 'MXN',
      conversion: 1,
      editing: false,
      editEmployeeName: '',
      editEmployeeSalary: 0,
      editRow: 0,
      photoId: 0,
      validate: false,
      takingPhoto: false
		};
	}

  // Formateo de dinero a XXXX.XX
  formatMoney = (amount, decimalCount = 2, decimal = ".", thousands = ",") => {
    try {
      decimalCount = Math.abs(decimalCount);
      decimalCount = isNaN(decimalCount) ? 2 : decimalCount;
  
      const negativeSign = amount < 0 ? "-" : "";
  
      let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
      let j = (i.length > 3) ? i.length % 3 : 0;
  
      return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
    } catch (e) {
      console.log(e)
    }
  };

  handleChange = (evt) => {
    const value = evt.target.value;

    this.setState({
      [evt.target.name]: value
    })
  }

  // Conversión entre Pesos y Dólares
  handleCurrency = (evt) => {
    var currency = this.state.currency;
    var conversion = this.state.conversion;

    var newCurrency = currency === 'MXN' ? 'USD' : 'MXN';
    var newConversion = conversion === 1 ? 21.50 : 1;

    this.setState({
        currency: newCurrency,
        conversion: newConversion
    });
  }

  // Mostrar el formulario para agregar nuevo empleado
  showEmployeeForm = (evt) => {
    var showEmployee = this.state.newEmployee;
    
    var showNewEmployee = showEmployee === true ? false : true;

    this.setState({
        newEmployee: showNewEmployee,
        newEmployeeName: '',
        newEmployeeEnterprise: '',
        newEmployeeSalary: '',
        validate: false
    });
  }

  // Añadir empleado
  addEmployee = (evt) => {
    if (this.state.newEmployeeName === '' || this.state.newEmployeeEnterprise === '' || this.state.newEmployeeSalary === '') {
      this.setState({
        validate: true
      });  
      return false;
    }
    
    let employeeEntry = {
      id: this.state.employees.length + 1,
      name: this.state.newEmployeeName,
      enterprise: this.state.newEmployeeEnterprise,
      salary: this.state.newEmployeeSalary,
      photo: "foto-template.png"
    }

    this.setState({
      employees:  this.state.employees.concat(employeeEntry),
      newEmployeeName: '',
      newEmployeeEnterprise: '',
      newEmployeeSalary: '',
      validate: false
    })

    employeeEntry = {}
  }

  // Edición de empleado
  editEmployee = (evt) => {
    this.setState({
      editing: true,
      editRow: evt.target.value,
      editEmployeeName: this.state.employees[evt.target.value - 1].name,
      editEmployeeSalary: this.state.employees[evt.target.value - 1].salary
    })

    console.log(this.state.employees[evt.target.value - 1].salary)
  }

  // Actualizar datos de empleado
  updateEmployee = (evt) => {
    if (this.state.editEmployeeName === '' || this.state.editEmployeeSalary === '') {
      this.setState({
        validate: true
      });  
      return false;
    }

    let employeeList = this.state.employees;

    employeeList[evt.target.value-1].name = this.state.editEmployeeName;
    employeeList[evt.target.value-1].salary = this.state.editEmployeeSalary;

    this.setState({
      employees:  employeeList,
      editing: false,
      editRow: 0,
      validate: false
    })
  }

  // Cancelar actualización
  cancelUpdate = (evt) => {
    this.setState({
      editing: false,
      editRow: 0,
      validate: false
    })
  }


  // Input para únicamente números
  // https://stackoverflow.com/questions/43715973/how-allow-only-numbers-and-2-digit-decimal-in-textbox-by-using-jquery
  onlyNumbersInput = (evt) => {
    var val = evt.target.value;
    var re = /^([0-9]+[.]?[0-9]?[0-9]?|[0-9]+)$/g;
    var re1 = /^([0-9]+[.]?[0-9]?[0-9]?|[0-9]+)/g;
    if (re.test(val)) {
      this.setState({
        newEmployeeSalary: val
      })
    } else {
      val = re1.exec(val);
      if (val) {
        evt.target.value = val[0];
      } else {
        evt.target.value = "";
      }
    }
  }


  // Consumo del JSON base de empleados
  componentDidMount = () => {
    fetch("employees.json")
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            employees: result.data.employees
          });
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
          alert("error");
        }
      )
  }


  // Inicializar cámara
  // https://mdn-samples.mozilla.org/s/webrtc-capturestill/
  startupCamera = () => {
    video = document.getElementById('video');
    canvas = document.getElementById('canvas');
    
    navigator.mediaDevices.getUserMedia({video: true, audio: false})
    .then(function(stream) {
      video.srcObject = stream;
      video.play();
    })
    .catch(function(err) {
      console.log("An error occurred: " + err);
    });

    video.addEventListener('canplay', function(ev){
      if (!streaming) {
        height = video.videoHeight / (video.videoWidth/width);
      
        if (isNaN(height)) {
          height = width / (4/3);
        }
      
        video.setAttribute('width', width);
        video.setAttribute('height', height);
        canvas.setAttribute('width', width);
        canvas.setAttribute('height', height);
        streaming = true;
      }
    }, false);
  }

  // Limpiar foto
  clearPhoto = () => {
    var photo = document.getElementById('photo'+this.state.photoId);

    var context = canvas.getContext('2d');
    context.fillStyle = "#AAA";
    context.fillRect(0, 0, canvas.width, canvas.height);

    var data = canvas.toDataURL('image/png');
    photo.setAttribute('src', data);
  }



  // Formulario de fotografía (MODAL)
  photoForm = (evt) => {
    if (this.state.photoId === 0){
      this.startupCamera()
    }

    this.setState({
      photoId: evt.target.value,
      takingPhoto: true
    })
  }
  
  // Tomar fotografía
  takePhoto = () => {
    var photo = document.getElementById('photo'+this.state.photoId);
    var context = canvas.getContext('2d');
    if (width && height) {
      canvas.width = width;
      canvas.height = height;
      context.drawImage(video, 0, 0, width, height);
    
      var data = canvas.toDataURL('image/png');
      console.log(photo);
      photo.setAttribute('src', data);
      this.setState({
        photoId: 0,
        takingPhoto: false
      })
    } else {
      this.clearPhoto();
    }

    navigator.mediaDevices.getUserMedia({video: true, audio: false})
    .then(function() {
      video.srcObject.getTracks()[0].stop();
      video.srcObject = null;
    })
    .catch(function(err) {
      console.log("An error occurred: " + err);
    });
  }

  // Cancelar toma de fotografía
  cancelPhoto = () => {
    this.setState({
      photoId: 0,
      takingPhoto: false
    })
    
    navigator.mediaDevices.getUserMedia({video: true, audio: false})
    .then(function() {
      video.srcObject.getTracks()[0].stop();
      video.srcObject = null;
    })
    .catch(function(err) {
      console.log("An error occurred: " + err);
    });
  }

  render() {
    // Filtrado: buscar coincidencias en los campos de empleados por medio del estado 'filter'
    const { filter, employees } = this.state;
    const lowercasedFilter = filter.toLowerCase();

    const filteredData = employees.filter(item => {
      return Object.keys(item).some(id =>
        item[id].toString().toLowerCase().includes(lowercasedFilter)
      );
    });
    return (
      <div className="App">
        <h1>Examen Brivé Soluciones 2021</h1>
        <div className="overflow-table">
          <table className="employee-table">
            <thead>
              <tr>
                <th colSpan="3"><p className="emp-paragraph"><span className="employees-num">{this.state.employees.length}</span> empleados</p></th>
                <th colSpan="3">Tipo de cambio: <span className="money-eq sub-emp-paragraph">{this.state.currency}</span></th>
              </tr>
              <tr>
                <th colSpan="3"><button className="btn" onClick={this.showEmployeeForm}>Nuevo empleado</button></th>
                <th colSpan="3"><button className="btn" onClick={this.handleCurrency}>Cambiar moneda</button></th>
              </tr>
              <tr className={this.state.newEmployee ? null: "disabled"}>
                <th colSpan="2"><p><label htmlFor="newEmployeeName">Nombre</label></p><input type="text" id="newEmployeeName" name="newEmployeeName" className={this.state.validate === true && this.state.newEmployeeName === '' ? "input-error": null} value={this.state.newEmployeeName} onChange={this.handleChange} /></th>
                <th colSpan="2"><p><label htmlFor="newEmployeeEnterprise">Empresa</label></p><input type="text" id="newEmployeeEnterprise" name="newEmployeeEnterprise" className={this.state.validate === true && this.state.newEmployeeEnterprise === '' ? "input-error": null} value={this.state.newEmployeeEnterprise} onChange={this.handleChange} /></th>
                <th><p><label htmlFor="newEmployeeSalary">Salario</label></p><input type="text" id="newEmployeeSalary" name="newEmployeeSalary" className={this.state.validate === true && this.state.newEmployeeSalary === '' ? "input-error": null} value={this.state.newEmployeeSalary} onChange={this.handleChange} onInput={this.onlyNumbersInput}/></th>
                <th><button className="btn" onClick={this.addEmployee}>Añadir Empleado</button></th>
              </tr>
            </thead>
            <tbody>
              <tr key="0">
                <th colSpan="6"><label htmlFor="filter">Filtrar</label> <input type="text" id="filter" name="filter" value={filter} onChange={this.handleChange} /></th>
              </tr>
              {
                filteredData.map((employee) => 
                <tr key={employee.id} className={ this.state.editing ? 'editing' : '' } >
                  <td>{parseInt(employee.id) === parseInt(this.state.editRow) ? <input type="text" name="editEmployeeName" className={this.state.validate === true && this.state.editEmployeeName === '' ? "input-error": null} value={this.state.editEmployeeName} onChange={this.handleChange} /> : employee.name}</td>
                  <td>{employee.enterprise}</td>
                  <td className={employee.salary < 10000 ? "warning money-col" : "stable money-col"} >{parseInt(employee.id) === parseInt(this.state.editRow) ? <div>$ <input type="text" name="editEmployeeSalary" className={this.state.validate === true && this.state.editEmployeeSalary === '' ? "input-error": null} value={this.state.editEmployeeSalary} onChange={this.handleChange} /> </div> : <p className="money-txt">$ {this.formatMoney(employee.salary/this.state.conversion)}</p>}</td>
                  <td><img className="photo-employee" id={"photo"+employee.id} src={employee.photo} alt={employee.name}/></td>
                  <td><button className="btn" value={employee.id} onClick={this.photoForm}>Tomar fotografía</button></td>
                  <td>{parseInt(employee.id) === parseInt(this.state.editRow) ? <div><button className="btn" value={employee.id} onClick={this.updateEmployee}>Guardar</button>&nbsp;<button className="btn" value={employee.id} onClick={this.cancelUpdate}>Cancelar</button></div>: <button className="btn" value={employee.id} onClick={this.editEmployee}>Editar datos</button> }</td>
                </tr>
                )
              }
            </tbody>
          </table>
        </div>
        <div className={this.state.takingPhoto ? "modal-cam show": "modal-cam"}>
          <div className="camera">
            <video id="video">Video stream not available.</video>
            <button className="btn" value={this.state.photoId} onChange={this.handleChange} id="startbutton" onClick={this.takePhoto}>Tomar fotografía</button>&nbsp;&nbsp;&nbsp;<button className="btn" value={this.state.photoId} onChange={this.handleChange} onClick={this.cancelPhoto}>Cancelar</button>
          </div>
          <canvas id="canvas">
          </canvas>
          {/* <div className="output">
            <img src="foto-template.png" id="photo" alt="The screen capture will appear in this box." /> 
          </div> */}
        </div>
      </div>
    );
  }
}

export default App;


